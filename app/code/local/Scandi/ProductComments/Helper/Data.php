<?php

/**
 * Scandi_ProductComments
 *
 * @category    Scandi
 * @package     Scandi_ProductComments
 * @author      Dmitrijs Kosjaks <info@scandiweb.com>
 * @copyright   Copyright (c) 2015 Scandiweb, Inc (http://scandiweb.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
class Scandi_ProductComments_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * Get comments collection
     *
     * @param $productId bool
     *
     * @return Scandi_ProductComments_Model_Resource_Comments_Collection
     */
    public function getCollection($productId = false)
    {
        $collection = array();
        $current    = Mage::registry('current_product');

        if ($productId || $current) {
            $collection = Mage::getModel('scandi_productcomments/comments')->getCollection()
                ->addFieldToFilter('status', array('in' => '1'))
                ->addFieldToFilter(
                    'product_id',
                    array('in' => $productId ? $productId : $current->getId())
                );
        }

        return $collection;
    }

    /**
     * Get single comment data.
     *
     * @param $comment object
     *
     * @return string
     */
    public function getCommentData($comment)
    {
        $helper = Mage::helper('scandi_productcomments');

        return '<h4>' . $helper->escapeHtml($comment->getName()) . ' - ' .
            $helper->escapeHtml($comment->getEmail()) . '</h4><p>' .
            $helper->escapeHtml($comment->getText()) . '</p>';
    }

    /**
     * Get url for comments form.
     *
     * @return string
     */
    public function getCommentsFormUrl()
    {
        return Mage::getUrl('productcomments/index/post');
    }
}