<?php
/**
 * Scandi_ProductComments
 *
 * @category    Scandi
 * @package     Scandi_ProductComments
 * @author      Dmitrijs Kosjaks <info@scandiweb.com>
 * @copyright   Copyright (c) 2015 Scandiweb, Inc (http://scandiweb.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
class Scandi_ProductComments_IndexController extends Mage_Core_Controller_Front_Action
{
    /**
     * Post action.
     */
    public function postAction()
    {
        $data   = $this->getRequest()->getPost();
        $helper = Mage::helper('scandi_productcomments');

        if (!empty($data)) {
            $translate = Mage::getSingleton('core/translate');
            /* @var $translate Mage_Core_Model_Translate */
            $translate->setTranslateInline(false);

            $post     = Mage::getModel('scandi_productcomments/comments')->setData($data);
            $validate = $post->validate();
            $session  = Mage::getSingleton('core/session');

            if ($validate === true) {
                try {
                    $post->save();
                    $translate->setTranslateInline(true);
                    $session->addSuccess($helper->__('Your comment was added.'));

                    $this->_redirectReferer();
                } catch (Exception $e) {
                    $translate->setTranslateInline(true);
                    $session->setFormData($data);
                    $session->addError($helper->__('Unable to post comment.'));

                    $this->_redirectReferer();
                }
            } else {
                $translate->setTranslateInline(true);
                $session->setFormData($data);

                if (is_array($validate)) {
                    foreach ($validate as $errorMessage) {
                        $session->addError($errorMessage);
                    }
                } else {
                    $session->addError($this->__('Unable to post comment.'));
                }

                $this->_redirectReferer();
            }
        } else {
            $this->_redirectReferer();
        }
    }
}