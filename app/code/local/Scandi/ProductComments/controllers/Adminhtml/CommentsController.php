<?php

/**
 * Scandi_ProductComments
 *
 * @category    Scandi
 * @package     Scandi_ProductComments
 * @author      Dmitrijs Kosjaks <info@scandiweb.com>
 * @copyright   Copyright (c) 2015 Scandiweb, Inc (http://scandiweb.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
class Scandi_ProductComments_Adminhtml_CommentsController extends Mage_Adminhtml_Controller_Action
{
    /**
     * Index action.
     */
    public function indexAction()
    {
        $helper = Mage::helper('scandi_productcomments');
        $this->_title($helper->__('Scandi / Product Comments'));
        $this->loadLayout();
        $this->_setActiveMenu('scandi');
        $this->_addBreadcrumb(
            $helper->__('Comments'),
            $helper->__('Comments')
        );
        $this->_addContent($this->getLayout()->createBlock('scandi_productcomments/adminhtml_comments'));
        $this->renderLayout();
    }

    public function newAction()
    {
        $this->_forward('edit');
    }

    /**
     * Edit comment action.
     */
    public function editAction()
    {
        $id     = (int)$this->getRequest()->getParam('id');
        $model  = Mage::getModel('scandi_productcomments/comments')->load($id);
        $helper = Mage::helper('scandi_productcomments');

        if ($id && !$model->getId()) {
            Mage::getSingleton('adminhtml/session')->addError(
                $helper->__('Comment does not exist.')
            );
            $this->_redirect('*/*/');

            return;
        }

        $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
        if (!empty($data)) {
            $model->setData($data);
        }

        $edit = $helper->__('Edit Comment');
        $new  = $helper->__('New Comment');

        $this->loadLayout();
        $this->_setActiveMenu('scandi');
        $this->_addBreadcrumb(
            $id ? $edit : $new,
            $id ? $edit : $new
        );
        $this->_addContent($this->getLayout()->createBlock('scandi_productcomments/adminhtml_comments_edit'));
        $this->_title($model->getId() ? $helper->__('Edit Comment') : $helper->__('New Comment'));
        $this->renderLayout();
    }

    /**
     * Delete comment action.
     */
    public function deleteAction()
    {
        $id     = $this->getRequest()->getParam('id');
        $helper = Mage::helper('scandi_productcomments');

        try {
            Mage::getModel('scandi_productcomments/comments')->setId($id)->delete();
            Mage::getSingleton('adminhtml/session')->addSuccess(
                $helper->__('Comment successfully deleted.')
            );

            return $this->_redirect('*/*/');
        } catch (Mage_Core_Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
        } catch (Exception $e) {
            Mage::logException($e);
            Mage::getSingleton('adminhtml/session')->addError(
                $helper->__('Could not delete comment.')
            );
        }

        $this->_redirectReferer();
    }

    /**
     * Save comment action.
     */
    public function saveAction()
    {
        $data   = $this->getRequest()->getPost();
        $helper = Mage::helper('scandi_productcomments');

        if (!empty($data)) {
            $post     = Mage::getModel('scandi_productcomments/comments')->setData($data);
            $validate = $post->validate();
            $session  = Mage::getSingleton('adminhtml/session');

            if ($validate === true) {
                try {
                    $post->save();
                    $session->addSuccess($helper->__('Comment successfully saved.'));
                } catch (Exception $e) {
                    Mage::log($e->getMessage(), null, 'product_comments.log');
                    $session->addError($e->getMessage() . '.<br/>' . $helper->__('Could not save comment.'));
                }
            } else {
                if (is_array($validate)) {
                    foreach ($validate as $errorMessage) {
                        $session->addError($errorMessage);
                    }
                } else {
                    $session->addError($this->__('Could not save comment.'));
                }
            }
        }

        if ($this->getRequest()->getParam('back')) {
            return $this->_redirect('*/*/edit', array('id' => $data['id']));
        } else {
            return $this->_redirect('*/*');
        }
    }

    /**
     * Mass delete of comments action.
     */
    public function massDeleteAction()
    {
        $ids    = $this->getRequest()->getParam('id');
        $helper = Mage::helper('scandi_productcomments');

        if (!is_array($ids) && empty($ids)) {
            Mage::getSingleton('adminhtml/session')->addError($helper->__('Please select items to delete.'));
        } else {
            try {
                foreach ($ids as $id) {
                    Mage::getModel('scandi_productcomments/comments')->load($id)->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    $helper->__('Total of %d record(s) were deleted.', count($ids))
                );
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }

        $this->_redirect('*/*/');
    }

    /**
     * Mass approve of comments action.
     */
    public function massApproveAction()
    {
        $ids    = $this->getRequest()->getParam('id');
        $helper = Mage::helper('scandi_productcomments');

        if (!is_array($ids) && empty($ids)) {
            Mage::getSingleton('adminhtml/session')->addError($helper->__('Please select items to approve.'));
        } else {
            try {
                foreach ($ids as $id) {
                    Mage::getModel('scandi_productcomments/comments')->load($id)->setStatus('1')->save();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    $helper->__('Total of %d record(s) were approved.', count($ids))
                );
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }

        $this->_redirect('*/*/');
    }

    /**
     * Mass disapprove of comments action.
     */
    public function massDisapproveAction()
    {
        $ids    = $this->getRequest()->getParam('id');
        $helper = Mage::helper('scandi_productcomments');

        if (!is_array($ids) && empty($ids)) {
            Mage::getSingleton('adminhtml/session')->addError($helper->__('Please select items to disapprove.'));
        } else {
            try {
                foreach ($ids as $id) {
                    Mage::getModel('scandi_productcomments/comments')->load($id)->setStatus('0')->save();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    $helper->__('Total of %d record(s) were disapproved.', count($ids))
                );
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }

        $this->_redirect('*/*/');
    }
}