<?php
/**
 * Scandi_ProductComments
 *
 * @category    Scandi
 * @package     Scandi_ProductComments
 * @author      Dmitrijs Kosjaks <info@scandiweb.com>
 * @copyright   Copyright (c) 2015 Scandiweb, Inc (http://scandiweb.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 *
 * @var $this Mage_Eav_Model_Entity_Setup
 */

// Adding "commenting" attribute
$this->removeAttribute(Mage_Catalog_Model_Product::ENTITY, 'commenting');
$this->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'commenting', array(
    'label'         => 'Allow comments',
    'group'         => 'General',
    'input'         => 'select',
    'type'          => 'int',
    'source'        => 'eav/entity_attribute_source_boolean',
    'visible'       => true,
    'user_defined'  => true,
    'required'      => false,
    'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
));

// Create table "scandi_productcomments/comments"
$table = $this->getConnection()
    ->newTable(
        $this->getTable('scandi_productcomments/comments')
    )
    ->addColumn(
        'id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'identity' => true,
            'nullable' => false,
            'primary'  => true,
        ), 'ID'
    )
    ->addColumn(
        'product_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'nullable' => false,
        ), 'Product ID'
    )
    ->addColumn(
        'name', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
            'nullable'  => false,
        ), 'Name'
    )
    ->addColumn(
        'status', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'nullable'  => false,
            'default'   => '0',
        ), 'Status'
    )
    ->addColumn(
        'email', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
            'nullable'  => false,
        ), 'Email'
    )
    ->addColumn(
        'text', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
            'nullable'  => false,
        ), 'Text'
    )
    ->addColumn(
        'date', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
            'nullable'  => false,
        ), 'Date'
    )
    ->addIndex($this->getIdxName(
            $this->getTable('scandi_productcomments/comments'),
            array('date'),
            Varien_Db_Adapter_Interface::INDEX_TYPE_INDEX
        ),
        array('date'),
        array('type' => Varien_Db_Adapter_Interface::INDEX_TYPE_INDEX)
    )
    ->addForeignKey(
        $this->getFkName(
            'scandi_productcomments/comments', 'product_id', 'catalog/product', 'entity_id'
        ),
        'product_id', $this->getTable('catalog/product'), 'entity_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE
    )
    ->setComment('Scandi Product Comments Table');

$this->getConnection()->createTable($table);