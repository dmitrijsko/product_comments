<?php
/**
 * Scandi_ProductComments
 *
 * @category    Scandi
 * @package     Scandi_ProductComments
 * @author      Dmitrijs Kosjaks <info@scandiweb.com>
 * @copyright   Copyright (c) 2015 Scandiweb, Inc (http://scandiweb.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
class Scandi_ProductComments_Block_Form extends Mage_Core_Block_Template
{
    /**
     * Prepare default template for comments.
     *
     * @return Scandi_ProductComments_Block_Form
     */
    protected function _prepareLayout()
    {
        $this->setTemplate('scandi/product_comments/form.phtml');
    }
}