<?php
/**
 * Scandi_ProductComments
 *
 * @category    Scandi
 * @package     Scandi_ProductComments
 * @author      Dmitrijs Kosjaks <info@scandiweb.com>
 * @copyright   Copyright (c) 2015 Scandiweb, Inc (http://scandiweb.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
class Scandi_ProductComments_Block_Adminhtml_Comments extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    /**
     * Block constructor.
     */
    protected function _construct()
    {
        $this->_addButtonLabel = Mage::helper('scandi_productcomments')->__('Add New Comment');
        $this->_blockGroup     = 'scandi_productcomments';
        $this->_controller     = 'adminhtml_comments';
        $this->_headerText     = Mage::helper('scandi_productcomments')->__('Comments');
    }
}