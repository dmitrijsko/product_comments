<?php
/**
 * Scandi_ProductComments
 *
 * @category    Scandi
 * @package     Scandi_ProductComments
 * @author      Dmitrijs Kosjaks <info@scandiweb.com>
 * @copyright   Copyright (c) 2015 Scandiweb, Inc (http://scandiweb.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
class Scandi_ProductComments_Block_Adminhtml_Comments_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    /**
     * Edit constructor.
     */
    protected function _construct()
    {
        $this->_controller = 'adminhtml_comments';
        $this->_objectId   = 'id';
        $this->_blockGroup = 'scandi_productcomments';
        $this->_mode       = 'edit';

        $this->_addButton('saveandcontinue', array(
            'label'   => Mage::helper('scandi_productcomments')->__('Save and Continue Edit'),
            'onclick' => 'saveAndContinueEdit()',
            'class'   => 'save',
        ), -100);
        $this->_formScripts[] = " function saveAndContinueEdit(){
            editForm.submit($('edit_form').action + 'back/edit/');}";

        $comment_id = (int)$this->getRequest()->getParam($this->_objectId);
        $comment    = Mage::getModel('scandi_productcomments/comments')->load($comment_id);
        Mage::register('current_comment', $comment);
    }

    /**
     * Retrieve text for header element depending on loaded page.
     *
     * @return string
     */
    public function getHeaderText()
    {
        $comment = Mage::registry('current_comment');
        $helper  = Mage::helper('scandi_productcomments');

        if ($comment->getId()) {
            return $helper->__("Edit Comment '%s'", $this->escapeHtml($comment->getName()));
        } else {
            return $helper->__("Add New Comment");
        }
    }

    /**
     * Getter of url for "Save and Continue" button.
     *
     * @return string
     */
    protected function _getSaveAndContinueUrl()
    {
        return $this->getUrl('*/*/save', array(
            '_current'   => true,
            'back'       => 'edit',
            'active_tab' => '{{tab_id}}',
        ));
    }
}
