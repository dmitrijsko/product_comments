<?php
/**
 * Scandi_ProductComments
 *
 * @category    Scandi
 * @package     Scandi_ProductComments
 * @author      Dmitrijs Kosjaks <info@scandiweb.com>
 * @copyright   Copyright (c) 2015 Scandiweb, Inc (http://scandiweb.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
class Scandi_ProductComments_Block_Adminhtml_Comments_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * Prepare form action.
     *
     * @return Scandi_ProductComments_Block_Adminhtml_Comments_Edit_Form
     */
    protected function _prepareForm()
    {
        $comment  = Mage::registry('current_comment');
        $form     = new Varien_Data_Form();
        $helper   = Mage::helper('scandi_productcomments');

        $fieldset = $form->addFieldset('edit_comment', array(
            'legend' => $helper->__('Comment Details')
        ));

        if ($comment->getId()) {
            $fieldset->addField('id', 'hidden', array(
                'name'      => 'id',
                'required'  => true,
            ));
        }

        $fieldset->addField('product_id', 'text', array(
            'name'      => 'product_id',
            'label'     => $helper->__('Product ID'),
            'required'  => true,
        ));

        $fieldset->addField('name', 'text', array(
            'name'      => 'name',
            'label'     => $helper->__('Author Name'),
            'required'  => true,
        ));

        $fieldset->addField('email', 'text', array(
            'name'      => 'email',
            'label'     => $helper->__('Author Email'),
            'required'  => true,
        ));

        $fieldset->addField('text', 'textarea', array(
            'name'      => 'text',
            'label'     => $helper->__('Comment'),
            'style'     => 'width: 98%; height: 200px;',
            'required'  => true,
        ));

        $fieldset->addField('status', 'select', array(
            'name'      => 'status',
            'label'     => $helper->__('Status'),
            'required'  => true,
            'options'   => array('0' => $helper->__('Not Approved'), '1' => $helper->__('Approved')),
        ));

        $form->setMethod('post');
        $form->setUseContainer(true);
        $form->setId('edit_form');
        $form->setAction($this->getUrl('*/*/save'));
        $form->setValues($comment->getData());

        $this->setForm($form);
    }
}