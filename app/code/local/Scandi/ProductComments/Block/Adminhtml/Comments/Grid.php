<?php
/**
 * Scandi_ProductComments
 *
 * @category    Scandi
 * @package     Scandi_ProductComments
 * @author      Dmitrijs Kosjaks <info@scandiweb.com>
 * @copyright   Copyright (c) 2015 Scandiweb, Inc (http://scandiweb.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
class Scandi_ProductComments_Block_Adminhtml_Comments_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * Grid constructor
     */
    protected function _construct()
    {
        $this->setId('commentsGrid');
        $this->setDefaultSort('date');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }

    /**
     * Add delete and status mass actions.
     *
     * @return Scandi_ProductComments_Block_Adminhtml_Comments_Grid
     */
    protected function _prepareMassaction()
    {
        $helper = Mage::helper('scandi_productcomments');

        $this->setMassactionIdField('id');
        $this->getMassactionBlock()->setFormFieldName('id');
        $this->getMassactionBlock()->addItem('delete', array(
            'label'   => $helper->__('Delete'),
            'url'     => $this->getUrl('*/*/massDelete'),
            'confirm' => $helper->__('Are you sure?'),
        ));
        $this->getMassactionBlock()->addItem('approved', array(
            'label' => $helper->__('Approved'),
            'url'   => $this->getUrl('*/*/massApprove'),
        ));
        $this->getMassactionBlock()->addItem('not_approved', array(
            'label' => $helper->__('Not Approved'),
            'url'   => $this->getUrl('*/*/massDisapprove'),
        ));

        return $this;
    }

    /**
     * Prepare collection for grid.
     *
     * @return Scandi_ProductComments_Block_Adminhtml_Comments_Grid
     */
    protected function _prepareCollection()
    {
        /* @var $collection Scandi_ProductComments_Model_Resource_Comments_Collection */
        $collection = Mage::getModel('scandi_productcomments/comments')
            ->getCollection();

        $prodNameAttrId = Mage::getModel('eav/entity_attribute')
            ->loadByCode(Mage_Catalog_Model_Product::ENTITY, 'name')
            ->getAttributeId();

        $collection->getSelect()
            ->joinLeft(
                array('product' => 'catalog_product_entity'),
                'product.entity_id = main_table.product_id',
                array('sku')
            )
            ->joinLeft(
                array('ev' => 'catalog_product_entity_varchar'),
                'ev.entity_id = product.entity_id AND ev.attribute_id = ' . $prodNameAttrId,
                array('product_name' => 'value')
            );

        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    /**
     * Prepare grid columns.
     *
     * @return Scandi_ProductComments_Block_Adminhtml_Comments_Grid
     */
    protected function _prepareColumns()
    {
        $helper = Mage::helper('scandi_productcomments');

        $this->addColumn('id', array(
            'header'        => $helper->__('ID'),
            'align'         => 'right',
            'width'         => '20px',
            'index'         => 'id',
        ));

        $this->addColumn('date', array(
            'header'        => $helper->__('Date'),
            'align'         => 'left',
            'index'         => 'date',
            'type'          => 'date',
            'escape'        => true,
        ));

        $this->addColumn('name', array(
            'header'        => $helper->__('Author Name'),
            'align'         => 'left',
            'index'         => 'name',
            'type'          => 'text',
            'truncate'      => 100,
            'escape'        => true,
        ));

        $this->addColumn('product_name', array(
            'header'        => $helper->__('Product Name'),
            'align'         => 'left',
            'index'         => 'product_name',
            'type'          => 'text',
            'truncate'      => 100,
            'escape'        => true,
        ));

        $this->addColumn('text', array(
            'header'        => $helper->__('Comment'),
            'align'         => 'left',
            'index'         => 'text',
            'type'          => 'text',
            'truncate'      => 200,
            'escape'        => true,
        ));

        $this->addColumn('status', array(
            'header'        => $helper->__('Status'),
            'align'         => 'left',
            'index'         => 'status',
            'type'          => 'options',
            'truncate'      => 200,
            'escape'        => true,
            'options'       => array('Not Approved', 'Approved'),
        ));

        $this->addColumn('action', array(
            'header'    => $helper->__('Action'),
            'width'     => '50px',
            'type'      => 'action',
            'getter'    => 'getId',
            'actions'   => array(
                array(
                    'caption' => $helper->__('Edit'),
                    'url'     => array(
                        'base'=>'*/*/edit',
                    ),
                    'field'   => 'id'
                )
            ),
            'filter'    => false,
            'sortable'  => false,
            'index'     => 'id',
        ));

        return parent::_prepareColumns();
    }

    /**
     * Return row url.
     *
     * @param $quote
     *
     * @return string
     */
    public function getRowUrl($quote)
    {
        return $this->getUrl('*/*/edit', array(
            'id' => $quote->getId(),
        ));
    }

    /**
     * Return grid url.
     *
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current' => true));
    }
}