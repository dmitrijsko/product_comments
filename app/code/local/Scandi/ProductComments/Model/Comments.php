<?php
/**
 * Scandi_ProductComments
 *
 * @category    Scandi
 * @package     Scandi_ProductComments
 * @author      Dmitrijs Kosjaks <info@scandiweb.com>
 * @copyright   Copyright (c) 2015 Scandiweb, Inc (http://scandiweb.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
class Scandi_ProductComments_Model_Comments extends Mage_Core_Model_Abstract
{
    /**
     * Define resource model.
     */
    protected function _construct()
    {
        $this->_init('scandi_productcomments/comments');
    }

    /**
     * If object is new adds creation time.
     *
     * @return Scandi_ProductComments_Model_Comments
     */
    protected function _beforeSave()
    {
        parent::_beforeSave();
        if ($this->isObjectNew()) {
            $this->setData('date', Varien_Date::now());
        }

        return $this;
    }

    /**
     * Validate for frontend/backend form fields
     *
     * @return array|bool
     */
    public function validate()
    {
        $errors = array();
        $helper = Mage::helper('scandi_productcomments');

        if (!Zend_Validate::is($this->getName(), 'NotEmpty')) {
            $errors[] = $helper->__('Name can\'t be empty.');
        }

        if (!Zend_Validate::is($this->getText(), 'NotEmpty')) {
            $errors[] = $helper->__('Text can\'t be empty.');
        }

        if (!Zend_Validate::is($this->getEmail(), 'EmailAddress')) {
            $errors[] = $helper->__('Enter valid email address.');
        }

        if (empty($errors)) {
            return true;
        }

        return $errors;
    }
}