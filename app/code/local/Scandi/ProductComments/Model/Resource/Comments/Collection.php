<?php
/**
 * Scandi_ProductComments
 *
 * @category    Scandi
 * @package     Scandi_ProductComments
 * @author      Dmitrijs Kosjaks <info@scandiweb.com>
 * @copyright   Copyright (c) 2015 Scandiweb, Inc (http://scandiweb.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
class Scandi_ProductComments_Model_Resource_Comments_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    /**
     * Define collection model.
     */
    protected function _construct()
    {
        $this->_init('scandi_productcomments/comments');
    }
}